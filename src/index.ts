import { post, get, put } from 'request-promise-native';
import {
    USERS_SERVICE_PORT_DEFAULT,
    AUTH_ENDPOINT,
    BASE_URL,
    REGISTER_ENDPOINT,
    GET_USER_ENDPOINT,
    UPDATE_USER_ENDPOINT,
    GET_USER_BY_ID,
    CACHE_PREFIX,
    ONE_HOUR_IN_SECONDS,
    RESTORE_ENDPOINT,
    GET_USER_CHILDREN,
    LOGOUT_ENDPOINT,
    REFRESH_TOKEN_ENDPOINT,
    CLONE_USER,
    GET_LAST_REGISTER_USERS,
} from './consts/system';
import { ICacheService } from './interfaces/cache-service';

export class UsersServiceAPI {
    private baseUrl = ''
    private headers = {
        'Content-Type': 'application/json',
        'User-Agent': '',
    }

    constructor(
        private readonly serviceName: string,
        private readonly cacheService: ICacheService,
        private readonly url?: string,
        private readonly port?: number,
    ) {
        this.headers['User-Agent'] = this.serviceName;
        this.baseUrl = `${url || BASE_URL}:${port || USERS_SERVICE_PORT_DEFAULT}`;
    }


    public async auth(login: string, password: string): Promise<any> {
        const result = await this.post(AUTH_ENDPOINT, {
            login,
            password,
        });

        return result;
    }

    public async register(params: any): Promise<any> {
        const result = await this.post(REGISTER_ENDPOINT, params);

        return result;
    }

    public async logout(token: string): Promise<any> {
        const result = await this.post(LOGOUT_ENDPOINT, {}, {
            'Authorization': token,
        });

        return result;
    }

    public async refreshToken(refreshToken: string): Promise<any> {
        const result = await this.post(REFRESH_TOKEN_ENDPOINT, { refreshToken });

        return result;
    }

    public async getUserInfo(token: string): Promise<any> {
        const result = await this.get({
            endpont: GET_USER_ENDPOINT,
            qs: {},
            headers: {
                'Authorization': token,
            },
        });

        return result;
    }

    public async updateUserInfo(token: string, info: object): Promise<any> {
        const result = await this.put(UPDATE_USER_ENDPOINT, info, {
            'Authorization': token,
        });

        return result;
    }

    public async updateUserInfoAdmin(token: string, userId: string, info: object): Promise<any> {
        const result = await this.put(`${UPDATE_USER_ENDPOINT}/${userId}`, info, {
            'Authorization': token,
        });

        return result;
    }

    public async getLastRegisterUsers(token: string, skip: number): Promise<any> {
        const result = await this.get({
            endpont: GET_LAST_REGISTER_USERS,
            qs: {skip},
            headers: {
                'Authorization': token,
            },
        });

        return result;
    }

    public async getUserChildren(token: string): Promise<any> {
        const result = await this.get({
            endpont: GET_USER_CHILDREN,
            qs: {},
            headers: {
                'Authorization': token,
            },
        });

        return result;
    }

    public async getUserInfoById(token: string, userId: string): Promise<any> {
        const params = {
            endpont: `${GET_USER_BY_ID}${userId}`,
            qs: {},
            headers: {
                'Authorization': token,
            },
        };
        const result = await this.checkCache(userId, this.get.bind(this), params)

        return result;
    }

    public async restorePassword(email: string): Promise<any> {
        const result = await this.post(RESTORE_ENDPOINT, { email });

        return result;
    }

    public async cloneUser(userId: string, params: any, internalToken: string) {
        const result = await this.post(`${CLONE_USER}${userId}`, params, {
            'InternalAuthorization': internalToken,
        });

        return result;
    }

    private async post(endpont: string, body: any, headers?: any) {
        try {
            const response = await post(
                `${this.baseUrl}${endpont}`,
                {
                    body,
                    headers: {
                        ...this.headers,
                        ...headers,
                    },
                    json: true,
                },
            );

            if (!response.result) {
                throw {
                    status: response.status,
                    message: response.message,
                }
            }

            return response.data;
        } catch (e) {
            throw {
                status: e.error.status,
                message: e.error.message,
            };
        }
    }

    private async put(endpont: string, body: any, headers?: any) {
        try {
            const response = await put(
                `${this.baseUrl}${endpont}`,
                {
                    body,
                    headers: {
                        ...this.headers,
                        ...headers,
                    },
                    json: true,
                },
            );

            if (!response.result) {
                throw {
                    status: response.status,
                    message: response.message,
                }
            }

            return response.data;
        } catch (e) {
            throw {
                status: e.error.status,
                message: e.error.message,
            };
        }
    }

    private async get({
        endpont,
        qs,
        headers,
    }: {
        endpont: string,
        qs: object,
        headers?: object,
    }) {
        try {
            const response = await get(
                `${this.baseUrl}${endpont}`,
                {
                    qs,
                    headers: {
                        ...this.headers,
                        ...headers,
                    },
                    json: true,
                },
            );

            if (!response.result) {
                throw {
                    status: response.status,
                    message: response.message,
                }
            }

            return response.data;
        } catch (e) {
            throw {
                status: e.error.status,
                message: e.error.message,
            };
        }
    }

    private async checkCache(key: string, cb: any, params: object) {
        const cacheExist = await this.cacheService.get(`${CACHE_PREFIX}${key}`);

        if (cacheExist) {
            return cacheExist;
        }

        const result = await cb(params);

        this.cacheService.save(`${CACHE_PREFIX}${key}`, result, ONE_HOUR_IN_SECONDS);

        return result;
    }
}