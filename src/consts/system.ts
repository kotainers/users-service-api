export const BASE_URL = 'http://localhost';
export const USERS_SERVICE_PORT_DEFAULT = 5001;
export const API_VER = 'v2';
export const BASE_API = `/api/users/${API_VER}/`;

export const CACHE_PREFIX = 'users_service_cache_';
export const ONE_HOUR_IN_SECONDS = 3600;
export const ONE_DAY_IN_SECONDS = 86400;

export const AUTH_ENDPOINT = `${BASE_API}account/login`;
export const REGISTER_ENDPOINT = `${BASE_API}account/register`;
export const GET_USER_ENDPOINT = `${BASE_API}account/me`;
export const BAN_USER_ENDPOINT = `${BASE_API}account/user/ban`;
export const UNBAN_USER_ENDPOINT = `${BASE_API}account/user/unban`;
export const UPDATE_USER_ENDPOINT = `${BASE_API}account/user`;
export const GET_USER_BY_ID = `${BASE_API}user/`;
export const RESTORE_ENDPOINT = `${BASE_API}account/restore`;
export const GET_USER_CHILDREN = `${BASE_API}account/me`;
export const GET_LAST_REGISTER_USERS = `${BASE_API}account/user/stats/register`;
export const LOGOUT_ENDPOINT = `${BASE_API}account/logout`;
export const REFRESH_TOKEN_ENDPOINT = `${BASE_API}account/refresh`;
export const CLONE_USER = `${BASE_API}internal/account/clone/`;
