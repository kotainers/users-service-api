export interface ICacheService {
    save(key: string, value: object, ttl?: number): void;
    get(key: string): Promise<any>;
}